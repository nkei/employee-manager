export class Employee {
    no?:number;
    firstName:string;
    lastName:string;
    gender:string;
    position?: number;
    failure:boolean;

    static Factory(no:number, firstName:string, lastName:string, position?:number, gender?:string) {
        return {no: no, firstName: firstName, lastName: lastName, position: position || 0, gender: gender, } as Employee;
    }
}