import * as React from "react";
import '../css/EmployeeManger.css';
import {Employee} from "./Employee";
import EmployeeList from "./EmployeeList";
import EmployeeForm from "./EmployeeForm";
import {EMPLOYEE_CURRENT, EMPLOYEE_DEBUG, EMPLOYEE_LIST_STORAGE} from "./consts";
import {EmployeeLocalStorageWorker as storage} from "./EmployeeLocalStorageWorker";

const TEST_EMPLOYEES = [
    {no: 0, firstName: "Frodo", lastName: "Baggins", gender: "male", position: 0, failure: false},
    {no: 1, firstName: "Sam", lastName: "Gamgee", gender: "male", position: 0, failure: false}
];

type EmployeeManagerProps = any;

interface EmployeeManagerState {
    employees: Employee[]
    selectedEmployee: Employee
    dataModify: boolean
    debug: boolean
}

///родительский компонент редактора справочника
class EmployeeManager extends React.Component<EmployeeManagerProps, EmployeeManagerState> {
    constructor(props) {
        super(props);
        this.state = {
            employees: storage.EMPLOYEES_RAW_STORAGE && storage.EMPLOYEES_RAW_STORAGE != "" ? storage.GetEmployees() : null || TEST_EMPLOYEES,
            selectedEmployee: null,
            dataModify: false,
            debug: EMPLOYEE_DEBUG
        };
        this.onSelectEmployeeHandle = this.onSelectEmployeeHandle.bind(this);
        this.onEnployeeChangedHandle = this.onEnployeeChangedHandle.bind(this);
    }

    componentWillMount(): void {
        //выбор первого сотрудника из списка
        if (this.state.selectedEmployee == null && this.state.employees != null && this.state.employees.length > 0) {
            this.onSelectEmployeeHandle(0);
        }
        //сохраняем состояние
        this.storageSave()
    }

    //сотрудник изменен в форме
    private onEnployeeChangedHandle(item){
        this.setState({dataModify: true})
    }

    //выбрать сотрудника
    private onSelectEmployeeHandle(id: number) {
        if (!this.state.employees || this.state.employees.length == 0)
            return;

        if (this.state.employees.length > id && id >= 0) {
            this.setState({selectedEmployee: this.state.employees[id]})
        }
        else {
            this.setState({selectedEmployee: this.state.employees[this.state.employees.length - 1]})
        }
    }

    //сохранение и загрузка списка сотрудников в локальное хранилище
    storageSave = () => storage.SetEmployees(this.state.employees);
    storageLoad = () => this.setState({employees: storage.GetEmployees(), dataModify: false});

    //сохранить изменения
    save() {
        if (confirm('Вы действительно хотите сохранить данные на сервер?')) {
            alert("Данные сохранены")
            this.storageSave()
        }
        this.setState({dataModify: false})
    }

    //обновить данные с сервера
    refresh() {

        if (this.state.dataModify && confirm('Есть не сохраненные даныне, обновить данные с сервера?')) {
            //здесь обновление с сервера
            this.storageLoad()
            this.onSelectEmployeeHandle(0)
            alert("Данные обновлены")
        }
    }

    //удалить сотрудника
    deleteSelectEmployee() {
        let name = this.state.selectedEmployee.firstName + " " + this.state.selectedEmployee.lastName;
        if (confirm("Удалить сотрудника \"" + name + "\"?")) {
            let newEmployeeList = this.state.employees;
            let deleteIndex = this.state.employees.indexOf(this.state.selectedEmployee);
            newEmployeeList.splice(deleteIndex, 1)
            this.setState({
                employees: newEmployeeList,
                dataModify: true
            }, () => {
                this.onSelectEmployeeHandle(deleteIndex)
            })
            alert(name + " удален")
        }
    }

    //добавить сотрудника
    newEmployee(){
        let newEmployee = Employee.Factory(this.getIncrementEmployeeNo(), "Новый", "Сотрудник")// {no: this.getIncrementEmployeeNo(), firstName: "Новый", lastName: "Сотрудник", gender: "", position: 0, failure: false};
        this.setState({employees: this.state.employees.concat(newEmployee), selectedEmployee: newEmployee},()=> this.onSelectEmployeeHandle( this.state.employees.length - 1))
    }

    //для теста создание локального инкремента для нового сотрудника
    getIncrementEmployeeNo() {
        let max = 0;
        this.state.employees.forEach((item: Employee) => {
            if (item.no > max)
                max = item.no;
        });
        return max + 1;
    }

    render() {
        let debug_block = EMPLOYEE_DEBUG ? <div className="debug">{JSON.stringify(this.state.selectedEmployee)}</div> : "";
        return (
            <div className="EmployeeManager">
                <header className="EmployeeManager-header">
                    <h1 className="EmployeeManager-title">Редактор справочника сотрудников</h1>
                </header>
                <div className="row">
                    <div className="col-4 EmployeeList">
                        <EmployeeList employees={this.state.employees} selectedEmployee={this.state.selectedEmployee} onSelectChange={this.onSelectEmployeeHandle} />
                    </div>
                    <div className="col-8 EmployeeForm" >
                        <EmployeeForm editEmployee={this.state.selectedEmployee} onFormUpdate={this.onEnployeeChangedHandle} />
                        <nav className="nav">
                            <div className="nav btn-group">
                                <button className="btn btn-primary" disabled={!this.state.dataModify} title="Сохранить все изменения на сервере" onClick={this.save.bind(this)}>Сохранить</button>
                                <button className="btn btn-primary" title="Обновить данные с сервера" onClick={this.refresh.bind(this)}>Обновить</button>
                                <button className="btn btn-primary" title="Удалить выбранного сотрудника" onClick={this.deleteSelectEmployee.bind(this)}>Удалить</button>
                                <button className="btn btn-primary" title="Добавить нового сотрудника" onClick={this.newEmployee.bind(this)}>Добавить</button>
                            </div>
                        </nav>
                    </div>
                </div>
                {debug_block}
            </div>
        )
    }
}
export default EmployeeManager;