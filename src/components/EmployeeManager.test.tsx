import * as React from 'react';
import * as ReactDOM from 'react-dom';
import EmployeeManager from './EmployeeManager';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EmployeeManager />, div);
  ReactDOM.unmountComponentAtNode(div);
});
