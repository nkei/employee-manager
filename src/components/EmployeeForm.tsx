import * as React from "react";
import {Employee} from "./Employee";
import {EMPLOYEE_DEBUG, EMPLOYEE_POSITIONS} from "./consts";

const DEFAULT_EMPLOYEE = {firstName: "", lastName: "", failure: false, position: 0, gender: "male"} as Employee;

interface EmployeeFormProps {
    editEmployee: Employee,
    onFormUpdate?:(Employee)=>void;
}

interface EmployeeFormState {
    editEmployee: Employee,
    firstName:string,
    lastName:string,
    gender: {
        male:boolean,
        female:boolean
    },
    failure:boolean,
    position:number
}

class EmployeeForm extends React.Component<EmployeeFormProps, EmployeeFormState>  {
    readonly Positions = EMPLOYEE_POSITIONS;
    static defaultProps = {
        editEmployee: DEFAULT_EMPLOYEE
    }

    constructor(props){
        super(props);

            this.state = {
                editEmployee: this.props.editEmployee,
                firstName: this.props.editEmployee.firstName,
                lastName: this.props.editEmployee.lastName,
                gender: {
                    male: this.props.editEmployee.gender === "male",
                    female: this.props.editEmployee.gender === "female"
                },
                failure:this.props.editEmployee.failure,
                position: this.props.editEmployee.position
            }

        this.updateState = this.updateState.bind(this);
    }
    componentWillReceiveProps(nextProps: Readonly<EmployeeFormProps>, nextContext: any): void {
        if(this.props != nextProps) {
            this.setState({
                editEmployee: nextProps.editEmployee,
                firstName: nextProps.editEmployee.firstName,
                lastName: nextProps.editEmployee.lastName,
                gender: {
                    male: nextProps.editEmployee.gender === "male",
                    female: nextProps.editEmployee.gender === "female"
                },
                failure: nextProps.editEmployee.failure,
                position: nextProps.editEmployee.position
            })
        }
        console.log("Will receive new props");
    }

    updateState(event){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let partialState = {};
        partialState[name] = value;
        this.setState(partialState, ()=>{
            let newEmployee = this.props.editEmployee;
            newEmployee[name] = value
            if (this.props.onFormUpdate) {
                this.props.onFormUpdate(newEmployee);
            }
        });
    }

    render(){
        let debug_block = EMPLOYEE_DEBUG ? <div className="debug">state:{JSON.stringify(this.state)}<br/>props:{JSON.stringify(this.props)}</div>:""
        return (
            <form>
                <div className="form-group">
                    <label htmlFor="firstName">Имя</label>
                    <input type="name" className="form-control" id="firstName" name="firstName" value={this.state.firstName} onChange={this.updateState}/>
                    <small id="firstHelp" className="form-text text-muted" />
                </div>
                <div className="form-group">
                    <label htmlFor="lastName">Фамилия</label>
                    <input type="name" className="form-control" id="lastName" name="lastName" value={this.state.lastName} onChange={this.updateState}/>
                    <small id="lastHelp" className="form-text text-muted" />
                </div>
                <div className="form-group">
                    <label htmlFor="position">Должность</label>
                    <select value={this.state.position} className="form-control" id="position" name="position" onChange={this.updateState}>
                        {this.Positions.map((item, index)=>{
                            return <option key={index} value={index}>{item}</option>
                        })};
                    </select>
                    <small id="positionHelp" className="form-text text-muted" />
                </div>
                <label className="form-check-label" >Пол</label>
                <div className="form-control-plaintext">
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="gender" id="male" value="male" checked={this.state.gender.male} onChange={this.updateState} />
                            <label className="form-check-label" htmlFor="male">Мужчина</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="gender" id="female" value="female" checked={this.state.gender.female} onChange={this.updateState} />
                            <label className="form-check-label" htmlFor="female">Женщина</label>
                    </div>
                </div>
                <div className="form-control-plaintext">
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="checkbox" id="failure" name="failure" value="failure" checked={this.state.failure} onChange={this.updateState} />
                        <label className="form-check-label" htmlFor="failure">Уволен</label>
                    </div>
                </div>

                {debug_block}
            </form>
        );
    }
}

export default EmployeeForm