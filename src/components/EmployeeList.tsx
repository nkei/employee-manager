import * as React from 'react';
import {Employee} from "./Employee";
import EmployeeListItem from "./EmployeeListItem";
import {EMPLOYEE_DEBUG} from "./consts";

interface Props {
    employees?: Employee[];
    selectedEmployee?: Employee;
    onSelectChange?: (number)=>void;
}

class EmployeeList extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
        this.listItemClickHandle = this.listItemClickHandle.bind(this);
    }

    listItemClickHandle(item: Employee) : void {
        if (this.props.onSelectChange){
            this.props.onSelectChange(this.props.employees.indexOf(item));
        }
    }

    render(){
        let debug_block = EMPLOYEE_DEBUG ? <div className="debug">selected: {JSON.stringify(this.props.selectedEmployee)}<br/>
            employees: {JSON.stringify(this.props.employees)}</div> : "";
        if (this.props.employees) {
            let items = this.props.employees.map((item, key)=> {
                return <EmployeeListItem key={key} employee={item} isActive={item === this.props.selectedEmployee} onClick={this.listItemClickHandle} />;
            });

            return (<div>
                    <ul className="list-group">{ items }</ul>
                    {debug_block}
                    </div>
                );
        }
        return <p>Список пуст</p>;
    }
}

export default EmployeeList;