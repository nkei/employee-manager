import {Employee} from "./Employee";
import {EMPLOYEE_LIST_STORAGE} from "./consts";

///работа с локальным хранилищем
export class EmployeeLocalStorageWorker {
    static EMPLOYEES_RAW_STORAGE = localStorage.EMPLOYEE_LIST_STORAGE;
    static CURRENT_RAW_STORAGE = localStorage.EMPLOYEE_CURRENT;

    static Set(key:string, value:any) {
        localStorage.setItem(key, JSON.stringify(value) )
    }
    static Get<T>(key:string): T {
        return JSON.parse( localStorage.getItem(key) );
    }
    static SetEmployees(employees:Employee[]) {
        this.Set(EMPLOYEE_LIST_STORAGE,employees);
    }
    static GetEmployees() :Employee[] {
        return this.Get<Employee[]>(EMPLOYEE_LIST_STORAGE) ;
    }
}