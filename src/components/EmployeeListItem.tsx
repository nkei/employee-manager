import * as React from "react";
import {Employee} from "./Employee";
import {EMPLOYEE_POSITIONS} from "./consts";

interface EmployeeListItemProps {
    employee: Employee,
    isActive?: boolean,
    onClick?: (Employee)=>void;
}
class EmployeeListItem extends React.Component<EmployeeListItemProps> {

    constructor(props){
        super(props);

        this.onClickHandle = this.onClickHandle.bind(this);
    }
    onClickHandle(){
        if (this.props.onClick)
            this.props.onClick(this.props.employee);
    }
    render(){
        let activeClass = this.props.isActive ? " active" : "";
        return <a href="#"
                       className={"list-group-item list-group-item-action" + activeClass}
                       id={"item-" + this.props.employee.no}
                  onClick={this.onClickHandle} >{this.props.employee.firstName} {this.props.employee.lastName}
            <br/><small>{EMPLOYEE_POSITIONS[this.props.employee.position]}</small>
            </a>};
}

export default EmployeeListItem;