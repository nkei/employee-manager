import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import EmployeeManager from "./components/EmployeeManager";

ReactDOM.render(
  <EmployeeManager />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();